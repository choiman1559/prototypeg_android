package com.gitlab.prototypeg.android;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.preference.PreferenceManager;

import com.github.megatronking.netbare.NetBare;
import com.github.megatronking.netbare.NetBareConfig;
import com.github.megatronking.netbare.http.HttpInjectInterceptor;
import com.github.megatronking.netbare.ssl.JKS;
import com.gitlab.prototypeg.Session;
import java.io.IOException;
import java.util.Arrays;

public class PrototypeG extends Application {
	final private Session session = new Session();

	final static String CHANNEL_VPN_SERVICE = "Vpn Service";

	final static String CHANNEL_BUILD = "Build";

	private JKS jks;

	private NetBareConfig.Builder configBuilder;

	@Override
	public void onCreate() {
		super.onCreate();

		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

		new Thread() {
			@Override
			public void run() {
				Session.init();
			}
		}.start();

		jks = new JKS(
				this, this.getString(R.string.app_name),
				this.getString(R.string.app_name).toCharArray(),
				this.getString(R.string.app_name),
				this.getString(R.string.app_name),
				this.getString(R.string.app_name),
				this.getString(R.string.app_name),
				this.getString(R.string.app_name)
		);

		if (!jks.isInstalled()) {
			try {
				JKS.install(this, this.getString(R.string.app_name), this.getString(R.string.app_name));
			} catch (IOException e){
				e.printStackTrace();
			}
		}

		configBuilder = NetBareConfig.defaultHttpConfig(jks, Arrays.asList(
				HttpInjectInterceptor.createFactory(
						new PacketInjector(session)
				)
		))
				.newBuilder();

		registerNotificationChannels();

		NetBare.get().attachApplication(this, false);

		session.getEventManager().registerListener(new EventListener(this));
		if (preferences.getBoolean(getString(R.string.inject_index_response), true)) {
			session.getNetworkManager().getResponseHandlerManager().registerHandler(new ResponseListener(this));
		}
	}

	public Session getSession() {
		return session;
	}

	public JKS getJKS() {
		return jks;
	}

	private void registerNotificationChannels() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			registerNotificationChannel(notificationManager, CHANNEL_VPN_SERVICE, NotificationManager.IMPORTANCE_LOW);
			registerNotificationChannel(notificationManager, CHANNEL_BUILD, NotificationManager.IMPORTANCE_DEFAULT);
		}
	}

	private void registerNotificationChannel(NotificationManager notificationManager, String channel, int importance) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			if (notificationManager.getNotificationChannel(channel) == null) {
				notificationManager.createNotificationChannel(
						new NotificationChannel(
								channel,
								channel,
								importance
						)
				);
			}
		}
	}

	public NetBareConfig getConfig() {
		return configBuilder
				.addAllowedApplication(PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.package_name), "none"))
				.build();
	}
}
