package com.gitlab.prototypeg.android;

import android.util.Log;

import androidx.annotation.NonNull;

import com.github.megatronking.netbare.http.HttpBody;
import com.github.megatronking.netbare.http.HttpRequest;
import com.github.megatronking.netbare.http.HttpResponse;
import com.github.megatronking.netbare.http.HttpResponseHeaderPart;
import com.github.megatronking.netbare.injector.InjectorCallback;
import com.github.megatronking.netbare.injector.SimpleHttpInjector;
import com.github.megatronking.netbare.stream.BufferStream;
import com.gitlab.prototypeg.Session;
import com.gitlab.prototypeg.network.request.Request;
import com.gitlab.prototypeg.network.request.RequestFactory;
import com.gitlab.prototypeg.network.response.Response;
import com.gitlab.prototypeg.network.response.ResponseFactory;

import org.apache.commons.httpclient.ChunkedInputStream;
import org.apache.commons.httpclient.ChunkedOutputStream;
import org.apache.commons.io.IOUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class PacketInjector extends SimpleHttpInjector {
	private Session session;
	private ByteArrayOutputStream buffer;
	private HttpResponseHeaderPart header;

	private Request request;

	public PacketInjector(Session session) {
		this.session = session;
	}

	@Override
	public boolean sniffResponse(@NonNull HttpResponse response) {
		if (response.isHttps()) return false;
		if (response.host().getHostName().equals("sn-list.girlfrontline.co.kr")) return false; //optimize
		return true;
	}

	@Override
	public boolean sniffRequest(@NonNull HttpRequest request) {
		if (request.isHttps()) return false;
		if (request.host().getHostName().equals("sn-list.girlfrontline.co.kr")) return false; //optimize
		return true;
	}

	@Override
	public void onRequestInject(@NonNull HttpRequest request, @NonNull HttpBody body, @NonNull InjectorCallback callback) throws IOException {
		callback.onFinished(body);
		Log.d("REQUEST", new String(body.toBuffer().array()));
		this.request = RequestFactory.get(request.path(), body.toBuffer().array());
		session.getNetworkManager().getRequestHandlerManager().handle(this.request);
	}

	@Override
	public void onResponseInject(@NonNull HttpResponseHeaderPart header, @NonNull InjectorCallback callback) throws IOException {
		Log.d("HEADERS", header.headers().toString());
		this.header = header;
		Log.d("URL", header.uri().toString());
		callback.onFinished(header);
		buffer = new ByteArrayOutputStream();
	}

	@Override
	public void onResponseInject(@NonNull HttpResponse httpResponse, @NonNull HttpBody body, @NonNull InjectorCallback callback) throws IOException {
		if (!"chunked".equals(header.header("Transfer-Encoding")) || !"gzip".equals(header.header("Content-Encoding"))) {
			Log.d("INDEX", new String(body.toBuffer().array()));
			callback.onFinished(body);
			return;
		}

		if (header.uri().getPath().equals("/index.php")) {
			//server has something wrong
			callback.onFinished(body);
			return;
		}

		byte[] bytes = body.toBuffer().array();
		buffer.write(bytes);
		if (new String(bytes).endsWith("\r\n\r\n")) {
			try {
				GZIPInputStream inputStream = new GZIPInputStream(
						new ChunkedInputStream(
								new ByteArrayInputStream(buffer.toByteArray())
						)
				);
				byte[] line = IOUtils.toByteArray(inputStream);
				inputStream.close();
				String uri = header.uri().getPath();
				Response response = ResponseFactory.get(
						uri.substring(Math.min(uri.length(), session.getURIHeader().length())),
						line,
						request
				);

				try {
					session.getNetworkManager().getResponseHandlerManager().handle(response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				byte[] newResponse;
				if (response.isEdited()) {
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					ChunkedOutputStream chunkedOutputStream = new ChunkedOutputStream(outputStream);
					GZIPOutputStream gzipOutputStream = new GZIPOutputStream(chunkedOutputStream);
					gzipOutputStream.write(response.getBuffer());
					gzipOutputStream.finish();
					chunkedOutputStream.finish();
					newResponse = outputStream.toByteArray();
					gzipOutputStream.close();
				} else {
					newResponse = buffer.toByteArray();
				}
				buffer.close();
				callback.onFinished(new BufferStream(ByteBuffer.wrap(newResponse)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
