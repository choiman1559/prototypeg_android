package com.gitlab.prototypeg.android;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Switch;

import com.crashlytics.android.Crashlytics;
import com.github.megatronking.netbare.NetBare;
import com.google.firebase.analytics.FirebaseAnalytics;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		//Crashlytics.getInstance().crash();
        if (NetBare.get().isActive()) {
			((Switch) findViewById(R.id.vpn_service)).setChecked(true);
		}
		((Switch) findViewById(R.id.vpn_service)).setOnCheckedChangeListener((view, isChecked) -> {
			Intent intent = NetBare.get().prepare();
			if (isChecked) {
				if (intent == null) {
					if (!NetBare.get().isActive()) {
						NetBare.get().start(((PrototypeG) getApplication()).getConfig());
					}
					startGF();
				} else {
					startActivityForResult(intent, 0);
				}
			} else {
				NetBare.get().stop();
			}
		});

		findViewById(R.id.start_gf).setOnClickListener((view) -> {
			startGF();
		});
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if (requestCode == 0) {
    		if (resultCode == RESULT_OK) {
    			if (!NetBare.get().isActive()) {
					NetBare.get().start(((PrototypeG) getApplication()).getConfig());
				}

    			startGF();
			} else {
				((Switch) findViewById(R.id.vpn_service)).setChecked(false);
    			final Intent intent = NetBare.get().prepare();
    			startActivityForResult(intent, 0);
			}
		}
    }

	private void startGF() {
		final Intent launchIntent = getPackageManager().getLaunchIntentForPackage(
				PreferenceManager.getDefaultSharedPreferences(this)
						.getString(getString(R.string.package_name), "")
		);
		if (launchIntent != null) {
			startActivity(launchIntent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == R.id.preference) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		return super.onOptionsItemSelected(item);
	}
}
