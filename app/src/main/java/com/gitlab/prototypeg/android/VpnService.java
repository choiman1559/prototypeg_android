package com.gitlab.prototypeg.android;

import android.app.Notification;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.github.megatronking.netbare.NetBareService;

public class VpnService extends NetBareService {
	@Override
	protected int notificationId() {
		return 1;
	}

	@NonNull
	@Override
	protected Notification createNotification() {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this, PrototypeG.CHANNEL_VPN_SERVICE)
				.setSmallIcon(android.R.drawable.ic_lock_idle_lock)
				.setContentTitle(getString(R.string.app_name))
				.setContentText("")
				.setPriority(NotificationCompat.PRIORITY_DEFAULT);
		return builder.build();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
