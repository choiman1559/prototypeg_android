package com.gitlab.prototypeg.android;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import com.gitlab.prototypeg.doll.Doll;
import com.gitlab.prototypeg.network.Handler;
import com.gitlab.prototypeg.network.response.BuildDollResponse;
import com.gitlab.prototypeg.network.response.IndexResponse;
import com.gitlab.prototypeg.network.response.Response;

import java.util.ArrayList;
import java.util.Random;

public class ResponseListener implements Handler<Response> {
	final SharedPreferences preferences;
	final private Context context;
	public ResponseListener(final Context context) {
		this.context = context;
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public void onBuildDollResponse(BuildDollResponse buildDollResponse) {
		System.out.println(buildDollResponse.getData().toString());
	}

	@Synchronized
	public void onIndexResponse(IndexResponse indexResponse) {
		try {
			if (preferences.getBoolean(context.getString(R.string.use_random_adjutant), false)) {
				randomizeAdjutant(indexResponse);
			}

			if (preferences.getBoolean(context.getString(R.string.attempt_decensor), false)) {
				decensor(indexResponse);
				preferences.edit().putBoolean(context.getString(R.string.attempt_decensor), false).apply();
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void randomizeAdjutant(IndexResponse indexResponse) {
		final ArrayList<Doll> collectedDolls = indexResponse.getUserInfo().getCollectedDolls();
		final ArrayList<Doll> blacklist = new ArrayList<>();
		final String[] blacklistData = preferences.getString(context.getString(R.string.blacklist), "").split("\\r?\\n");
		for (Doll doll:
				collectedDolls) {
			for (String id:
					blacklistData ) {
				try {
					if (doll.getId() == Short.valueOf(id)) {
						blacklist.add(doll);
						break;
					}
				} catch (NumberFormatException e) {}
			}
		}
		collectedDolls.removeAll(blacklist);

		final Random random = new Random();
		Doll doll;
		int bound = collectedDolls.size();
		if (preferences.getBoolean(context.getString(R.string.include_kalina), true)) {
			//bound++;
		}
		doll = collectedDolls.get(random.nextInt(bound));
		ArrayList<Short> skins = (ArrayList<Short>)doll.getSkins().clone();
		skins.retainAll(indexResponse.getSkinsWithUser().keySet());
		indexResponse.getUserRecord().getAdjutant().setDollId(doll.getId());
		if (skins.size() != 0) {
			indexResponse.getUserRecord().getAdjutant().setSkinNumber(skins.get(random.nextInt(skins.size())));
		} else {
			indexResponse.getUserRecord().getAdjutant().setSkinNumber((short) 0);
		}
		if (preferences.getBoolean(context.getString(R.string.use_damaged), false)) {
			indexResponse.getUserRecord().getAdjutant().setDamaged(random.nextBoolean());
		}
		indexResponse.setEdited(true);
	}

	private void decensor(IndexResponse indexResponse) {
		indexResponse.setDecensorFormula("30:30:30:30");
		indexResponse.setEdited(true);
	}
}
