package com.gitlab.prototypeg.android;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {

    }

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
    }
}