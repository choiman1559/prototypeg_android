package com.gitlab.prototypeg.android;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.preference.PreferenceManager;

import com.gitlab.prototypeg.data.Language;
import com.gitlab.prototypeg.event.DecodeFailedEvent;
import com.gitlab.prototypeg.event.DollBuildEvent;

import org.apache.commons.lang3.StringUtils;

public class EventListener implements java.util.EventListener {
	final private Context context;
	public EventListener(final Context context) {
		this.context = context;
	}

	public void onDollBuild(DollBuildEvent dollBuildEvent) {
		NotificationManagerCompat.from(context).notify(
				0,
				new NotificationCompat.Builder(context, PrototypeG.CHANNEL_BUILD)
						.setSubText(context.getString(R.string.doll_build))
						.setContentTitle(
								dollBuildEvent.getDoll().getName(
										toLanguage(
												PreferenceManager.getDefaultSharedPreferences(context)
														.getString(context.getString(R.string.language), "NONE")
										)
								)
						)
						.setContentText(StringUtils.repeat("★", dollBuildEvent.getDoll().getRank()))
						.setSmallIcon(android.R.drawable.ic_dialog_info)
						.setPriority(NotificationCompat.PRIORITY_DEFAULT)
						.build()
		);
	}

	public void onDataPacketDecodeFailed(DecodeFailedEvent event) {
		event.getThrowable().printStackTrace();
		new Handler(Looper.getMainLooper()).post(() -> {
			Toast.makeText(context, R.string.key_exchange_failed, Toast.LENGTH_LONG).show();
		});
	}

	public Language toLanguage(String language) {
		switch (language) {
			case "CN":
				return Language.CN;
			case "KR":
				return Language.KR;
			case "EN":
				return Language.EN;
			case "JP":
				return Language.JP;
			default:
				return Language.NONE;
		}
	}
}
