package com.gitlab.prototypeg.android;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_activity);
		Bundle bundle = new Bundle();
		SettingsFragment settingsFragment = new SettingsFragment();
		ArrayList<String> packageNames = new ArrayList<>();
		packageNames.add(getString(R.string.target_cn_uc));
		packageNames.add(getString(R.string.target_cn_bili));
		packageNames.add(getString(R.string.target_en));
		packageNames.add(getString(R.string.target_jp));
		packageNames.add(getString(R.string.target_tw));
		packageNames.add(getString(R.string.target_kr));
		ArrayList<CharSequence> p2 = new ArrayList<>();
		for (String s : packageNames) {
			try {
				getPackageManager().getPackageInfo(s, 0);
				p2.add(s);
			} catch (PackageManager.NameNotFoundException e) {
			}
		}
		CharSequence[] entries = new CharSequence[p2.size()];
		entries = p2.toArray(entries);
		bundle.putCharSequenceArray("package_names", entries);
		settingsFragment.setArguments(bundle);
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.settings, settingsFragment)
				.commit();

		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowCustomEnabled(true);
		}
	}

	public static class SettingsFragment extends PreferenceFragmentCompat {
		@Override
		public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
			final Bundle bundle = getArguments();
			setPreferencesFromResource(R.xml.root_preferences, rootKey);
			ListPreference listPreference = findPreference(getString(R.string.package_name));
			listPreference.setEntries(bundle.getCharSequenceArray("package_names"));
			listPreference.setEntryValues(bundle.getCharSequenceArray("package_names"));
		}

		@Override
		public boolean onPreferenceTreeClick(Preference preference) {
			return super.onPreferenceTreeClick(preference);
		}
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}